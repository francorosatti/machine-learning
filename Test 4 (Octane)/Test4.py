###############################################################################
# Name: Test4.py
# Desc: Octano. Se imprimen las curvas de aprendizaje y validacion.
# Auth: Franco Rosatti
# Date: Nov-2019
# Vers: 1.0
###############################################################################

import pandas as pd
import numpy as np
import Plotter as pl
from keras.models import Sequential
from keras.layers import Dense


def print_weights():
    for layer in model.layers:
        weights = layer.get_weights()
        print(weights)


print('loading data...')
# load the dataset
dataset = pd.read_csv('octane.csv', delimiter=',', header=None)

# split into input (x) and output (y) variables
x_train = dataset.to_numpy()[:, 0:4]
y_train = dataset.to_numpy()[:, 4]

print('creating model...')
# define the keras model
model = Sequential()
# input_dim define la cantidad de entradas
model.add(Dense(32, input_dim=4, activation='relu'))
# model.add(Dense(4, activation='relu'))
# El primer parametro es la cantidad de neuronas
model.add(Dense(1))

# relu = rectified linear unit activation function

print('compiling model...')
model.compile(loss='mean_squared_error',
              optimizer='adam',
              metrics=['mse'])

print('training model...')
# fit the keras model on the dataset
h = model.fit(x_train, y_train, epochs=4096,
              batch_size=4, validation_split=0.3,
              verbose=2)

# evaluate the keras model
loss, mse = model.evaluate(x_train, y_train, verbose=0)
print('Accuracy: %.2f' % mse)
print('Loss: %.2f' % loss)

print(model.summary())

print_weights()

# make class predictions with the model
a = np.array([55.33, 1.72, 54, 1.66219]).reshape(1, 4)
print("Prediccion #1: %.2f" % model.predict(a))
print("Resultado esperado #1: 92.19")
print("")

# 66.61,2.31,52,1.77967,91.90
a = np.array([66.61, 2.31, 52, 1.77967]).reshape(1, 4)
print("Prediccion #2: %.2f" % model.predict(a))
print("Resultado esperado #2: 91.90")
print("")

# 4.23,10.76,41,2.17070,97.61
a = np.array([4.23, 10.76, 41, 2.17070]).reshape(1, 4)
print("Prediccion #3: %.2f" % model.predict(a))
print("Resultado esperado #3: 97.61")
print("")

# 54.12,0.88,57,1.57818,92.17
a = np.array([54.12, 0.88, 57, 1.57818]).reshape(1, 4)
print("Prediccion #4: %.2f" % model.predict(a))
print("Resultado esperado #4: 92.17")

# pl.plotLCs(h)
pl.plotLoss(h)
