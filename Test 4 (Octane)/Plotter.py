##############################################################
# Name: LCPlotter.py
# Desc: Grafica las curvas de aprendizaje de los algoritmos
# Auth: Franco Rosatti
# Date: Nov-2019
# Vers: 1.0
##############################################################

import matplotlib.pyplot as plt


def plotLCs(history):
    for k in history.history.keys():
        plt.plot(history.history[k])
        # plt.plot(history.history['loss'])
        # plt.plot(history.history['val_loss'])
        plt.title('model %s' % k)
        plt.ylabel(k)
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.show()


def plotLoss(history):
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
