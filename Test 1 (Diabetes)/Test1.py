###############################################################################
# Name: Test1.py
# Desc: Primera prueba de redes con datos de diabetes.
# Auth: Franco Rosatti
# Date: Nov-2019
# Vers: 1.0
###############################################################################

import pandas as pd
from keras.models import Sequential
from keras.layers import Dense

print('loading data...')
# load the dataset
dataset = pd.read_csv('pima-indians-diabetes.data.csv', delimiter=',')

# split into input (x) and output (y) variables
x_train = dataset.to_numpy()[:600, 0:8]
y_train = dataset.to_numpy()[:600, 8]

x_test = dataset.to_numpy()[600:, 0:8]
y_test = dataset.to_numpy()[600:, 8]

print('creating model...')
# define the keras model
model = Sequential()
# input_dim define la cantidad de entradas
model.add(Dense(12, input_dim=8, activation='relu'))
# El primer parametro es la cantidad de neuronas
model.add(Dense(8, activation='relu'))
model.add(Dense(1, activation='sigmoid'))

# relu = rectified linear unit activation function

print('compiling model...')
# compile the keras model
model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

print('training model...')
# fit the keras model on the dataset
model.fit(x_train, y_train, epochs=150, batch_size=10, verbose=0)

# evaluate the keras model
loss, accuracy = model.evaluate(x_train, y_train, verbose=0)
print('Accuracy: %.2f' % (accuracy * 100))
print('Loss: %.2f' % loss)

# make class predictions with the model
predictions = model.predict_classes(x_test)

ok = 0
nok = 0
for i in range(x_test.shape[0]):
    if y_test[i] == predictions[i]:
        ok += 1
    else:
        nok += 1

print('OK: %.2f' % (ok * 100 / (ok + nok)))
print('NOT OK: %.2f' % (nok * 100 / (ok + nok)))

# see model
print(model.summary())
