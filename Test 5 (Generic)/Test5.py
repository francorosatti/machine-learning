###############################################################################
# Name: Test5.py
# Desc: Red generica, con configuracion
# Auth: Franco Rosatti
# Date: Nov-2019
# Vers: 1.0
###############################################################################

import pandas as pd
import numpy as np
import Plotter as pl
from keras.models import Sequential
from keras.layers import Dense

##################################################################
# Configuracion de la red

# Datos
DATA_PATH = 'octane.csv'
DATA_HEADER = None
DATA_DELIMITER = ','

# Modelo
INPUTS = 4
OUTPUTS = 1
HIDDEN_LAYER_NEURONS = 64
LOSS = 'mean_squared_error'
OPTIMIZER = 'adam'
METRICS = ['mse', 'mae']

# Entrenamiento
EPOCHS = 512
BATCH_SIZE = 32
TEST_PERC = 0.1  # porcentaje de los datos que se usa para validacion

# Visualizacion
PRINT_MODEL = False
PRINT_WEIGHTS = False
PLOT_LC = False  # LC = Learning Curves
PLOT_LOSS = True

# Prediccion
NUM_OF_PREDICTIONS = 4
##################################################################


def print_weights():
    for layer in model.layers:
        weights = layer.get_weights()
        print(weights)


print('loading data...')
# load the dataset
dataset = pd.read_csv(DATA_PATH, delimiter=DATA_DELIMITER, header=DATA_HEADER)

# split into input (x) and output (y) variables
x_train = dataset.to_numpy()[:, 0:INPUTS]
y_train = dataset.to_numpy()[:, INPUTS:INPUTS + OUTPUTS]

print('creating model...')
# define the keras model
model = Sequential()
# input_dim define la cantidad de entradas
model.add(Dense(HIDDEN_LAYER_NEURONS, input_dim=INPUTS, activation='relu'))
# El primer parametro es la cantidad de neuronas
model.add(Dense(OUTPUTS))

print('compiling model...')
model.compile(loss=LOSS,
              optimizer=OPTIMIZER,
              metrics=METRICS)

print('training model...')
# fit the keras model on the dataset
h = model.fit(x_train, y_train, epochs=EPOCHS,
              batch_size=BATCH_SIZE, validation_split=TEST_PERC,
              verbose=0)

# evaluate the keras model
metrics = np.zeros(len(METRICS) + 1)
metrics = model.evaluate(x_train, y_train, verbose=0)

print('loss: %.2f' % metrics[0])
i = 1
for m in METRICS:
    print('%s: %.2f' % (m, metrics[i]))
    i += 1

for x in range(NUM_OF_PREDICTIONS):
    index = np.random.choice(x_train.shape[0], 1, replace=False)
    # make class predictions with the model
    print("Prediccion #%d: %.2f" % (index, model.predict(
                                    x_train[index].reshape(1, INPUTS))))
    print("Realidad   #%d: %.2f" % (index, y_train[index]))
    print("")

if PRINT_MODEL:
    print(model.summary())

if PRINT_WEIGHTS:
    print_weights()

if PLOT_LC:
    pl.plotLCs(h)

if PLOT_LOSS:
    pl.plotLoss(h, TEST_PERC > 0)
