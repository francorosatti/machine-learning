###############################################################################
# Name: Test2.py
# Desc: Prueba con datos de una compuerta, logra predecirla al 100%
# Auth: Franco Rosatti
# Date: Nov-2019
# Vers: 1.0
###############################################################################

import pandas as pd
import numpy as np
from keras.models import Sequential
from keras.layers import Dense

print('loading data...')
# load the dataset
dataset = pd.read_csv('abc.csv', delimiter=',', header=None)

# split into input (x) and output (y) variables
x_train = dataset.to_numpy()[:, 0:3]
y_train = dataset.to_numpy()[:, 3]

print('creating model...')
# define the keras model
model = Sequential()
# input_dim define la cantidad de entradas
model.add(Dense(64, input_dim=3, activation='relu'))
# El primer parametro es la cantidad de neuronas
model.add(Dense(1, activation='sigmoid'))

# relu = rectified linear unit activation function

print('compiling model...')
# compile the keras model
# model.compile(loss='binary_crossentropy',
#              optimizer='adam',
#              metrics=['accuracy'])

model.compile(loss='mean_squared_error',
              optimizer='adam',
              metrics=['binary_accuracy'])

print('training model...')
# fit the keras model on the dataset
model.fit(x_train, y_train, epochs=128, verbose=1)

# evaluate the keras model
loss, accuracy = model.evaluate(x_train, y_train, verbose=0)
print('Accuracy: %.2f' % (accuracy * 100))
print('Loss: %.2f' % loss)

# print(model.summary())

# print_weights()

# make class predictions with the model
a = np.array([0, 0, 1]).reshape(1, 3)

print(model.predict(a).round())


# print(predictions)


def print_weights():
    for layer in model.layers:
        weights = layer.get_weights()
        print(weights)
